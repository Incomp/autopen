package me.incomp.autopen.cmd;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.incomp.autopen.Autopen;
import me.incomp.autopen.core.CRManager;
import me.incomp.autopen.core.CannedResponse;
import me.incomp.autopen.core.Perm;
import me.incomp.pith.format.FormatType;
import me.incomp.pith.format.Formatter;
import me.incomp.pith.sk89qcmd.Command;
import me.incomp.pith.sk89qcmd.CommandContext;
import me.incomp.pith.sk89qcmd.NestedCommand;

/**
 * Manages "/autopen", the base command.
 *
 * @author Incomp
 * @since Jan 4, 2017
 */
public class CmdAutopen {
	public static class CmdAutopenParent {
		@Command(aliases = {"autopen", "cannedresponses", "cannedreplies", "cr", "ap"}, desc = "The base command for Autopen.", min = -1, max = -1)
		@NestedCommand(CmdAutopen.class)
		public static void cmd(final CommandContext cn, CommandSender sn) throws CommandException {}
	}
	
	@Command(aliases = {"use", "u"}, desc = "Allows for the usage of a canned response.", usage = "<response> <targetPlayer>", min = 1, max = 2)
	public static void cmdUse(final CommandContext cn, CommandSender sn) throws CommandException {
		// TODO
		if(!(sn instanceof Player)){
			sn.sendMessage(Formatter.format("Autopen", "Only players may use Canned Responses.", FormatType.WARNING));
			return;
		}
		
		final Player p = (Player) sn;
		
		if(!p.hasPermission(Perm.USE.getNode())){
			p.sendMessage(Formatter.format("Autopen", "You don't have access to use Canned Responses.", FormatType.WARNING));
			return;
		}
		
		final String label = cn.getString(0);
		final CRManager crman = Autopen.getInstance().getCRManager();
		final CannedResponse cr = crman.getCannedResponse(label);
		
		if(cr == null){
			p.sendMessage(Formatter.format("Autopen", "No response found by &e\"" + label + "\" &7.", FormatType.WARNING));
			if(p.hasPermission(Perm.LIST.getNode())){
				p.sendMessage(Formatter.format("Autopen", "Use &e/autopen list &7to list all Canned Responses.", FormatType.WARNING));
			}
			return;
		}
		
		if(cr.requiresPlayer()){
			// TODO
			if(cn.argsLength() < 2){
				p.sendMessage(Formatter.format("Autopen", "The Canned Response &e\"" + label + "\" &7requires a player argument.", FormatType.WARNING));
				return;
			}
			
			final String targetName = cn.getString(1);
			final Player target = Bukkit.getPlayer(targetName);
			
			if(target == null || !target.isOnline()){
				p.sendMessage(Formatter.format("Autopen", "No online player found by the name of &e\"" + targetName + "\"&7.", FormatType.WARNING));
				return;
			}
			
			final String message = cr.getMessage(target);
			p.chat(message);
			return;
		}
		
		final String message = cr.getMessage(null);
		p.chat(message);
	}
	
	@Command(aliases = {"reload", "r"}, desc = "Reloads the plugin.", min = 0, max = 0)
	public static void cmdReload(final CommandContext cn, CommandSender sn) throws CommandException {
		if(sn.hasPermission(Perm.RELOAD.getNode())){
			Autopen.getInstance().reloadConfig();
			sn.sendMessage(Formatter.format("Autopen", "You successfully reloaded the plugin.", FormatType.NORMAL));
			return;
		}else{
			sn.sendMessage(Formatter.format("Autopen", "You don't have access to reload the plugin.", FormatType.WARNING));
			return;
		}
	}
	
	@Command(aliases = {"list", "l"}, desc = "Lists all available canned responses.", min = 0, max = 1)
	public static void cmdList(final CommandContext cn, CommandSender sn) throws CommandException {
		if(!(sn instanceof Player)){
			sn.sendMessage(Formatter.format("Autopen", "Only players may list Canned Responses.", FormatType.WARNING));
			return;
		}
		
		final Player p = (Player) sn;
		
		if(!p.hasPermission(Perm.LIST.getNode())){
			p.sendMessage(Formatter.format("Autopen", "You don't have access to list Canned Responses.", FormatType.WARNING));
			return;
		}
		
		final CRManager crm = Autopen.getInstance().getCRManager();
		
		if(crm.getCannedResponses().isEmpty()){
			p.sendMessage(Formatter.format("Autopen", "There are no Canned Responses loaded. Get to work.", FormatType.WARNING));
			return;
		}
		
		// Handle specific listings.
		if(cn.argsLength() == 1){
			final String label = cn.getString(0);
			
			if(!crm.isCannedResponse(label)){
				p.sendMessage(Formatter.format("Autopen", "I couldn't find a Canned Response with a label of &e\"" + label + "\" &7.", FormatType.WARNING));
				p.sendMessage(Formatter.format("Autopen", "To list all Canned Responses, use &e/autopen list &7.", FormatType.WARNING));
				return;
			}
			
			final CannedResponse cr = crm.getCannedResponse(label);
			
			p.sendMessage(Formatter.getLineRule());
			p.sendMessage(Formatter.format("Autopen", "&e" + label + " &7has the following responses.", FormatType.NORMAL));
			p.sendMessage(Formatter.getLineRule());
			for(String msg : cr.getContents()){
				p.sendMessage(Formatter.color("&2• &a\"" + msg + "\""));
			}
			p.sendMessage(Formatter.getLineRule());
			return;
		}
		
		// List all Canned Responses.
		p.sendMessage(Formatter.getLineRule());
		p.sendMessage(Formatter.format("Autopen", "The following Canned Responses are loaded and available for use.", FormatType.NORMAL));
		p.sendMessage(Formatter.getLineRule());
		for(CannedResponse cr :crm.getCannedResponses()){
			p.sendMessage(Formatter.color("&2• &a" + cr.getLabel()));
		}
		p.sendMessage(Formatter.getLineRule());
		p.sendMessage(Formatter.format("Autopen", "Type &e/autopen list &6<label> &7to view all responses under a label.", FormatType.NORMAL));
		p.sendMessage(Formatter.getLineRule());
		return;
	}
}
