package me.incomp.autopen;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import me.incomp.autopen.cmd.CmdAutopen.CmdAutopenParent;
import me.incomp.autopen.core.CRManager;
import me.incomp.pith.format.FormatType;
import me.incomp.pith.format.Formatter;
import me.incomp.pith.sk89qcmd.CommandException;
import me.incomp.pith.sk89qcmd.CommandPermissionsException;
import me.incomp.pith.sk89qcmd.CommandUsageException;
import me.incomp.pith.sk89qcmd.CommandsManager;
import me.incomp.pith.sk89qcmd.MissingNestedCommandException;
import me.incomp.pith.sk89qcmd.WrappedCommandException;
import me.incomp.pith.sk89qcmd.bukkit.CommandsManagerRegistration;

/**
 * The primary class for Autopen, a Spigot plugin.
 *
 * @author Incomp
 * @since Nov 8, 2016
 */
public class Autopen extends JavaPlugin {
	
	private static Autopen INSTANCE;
	
	// sk89q's command framework
	private CommandsManager<CommandSender> commands;
	private CommandsManagerRegistration cmdReg;
	
	// Core
	private CRManager crManager;
	
	public void onEnable(){
		// TODO: Edit as necessary
		INSTANCE = this;
		
		Formatter.log("Alright. Let's do this.", Level.INFO);
		Formatter.log("Running command setup.", Level.INFO);
		this.commands = new CommandsManager<CommandSender>() {
			@Override
			public boolean hasPermission(CommandSender sender, String perm) {
				return sender instanceof ConsoleCommandSender || sender.hasPermission(perm);
			}
		};
		this.cmdReg = new CommandsManagerRegistration(this, this.commands);
		this.setupCommands();
		
		this.saveDefaultConfig();
		
		Formatter.log("Running core feature setup.", Level.INFO);
		this.crManager = new CRManager(this);
		Formatter.log("Startup protocol concludes. This is pretty fancy, right?", Level.INFO);
	}
	
	public void onDisable(){
		// TODO: Edit as necessary
		Formatter.log("Time to pack my bags.", Level.INFO);
		HandlerList.unregisterAll(this);
		
		Formatter.log("Unregistered all listeners.", Level.INFO);
		this.cmdReg.unregisterCommands();
		
		Formatter.log("Unregistered all commands.", Level.INFO);
		Formatter.log("See you next time.", Level.INFO);
		
		INSTANCE = null;
		this.crManager = null;
	}
	
	/**
	 * Fetches the instance for this class.
	 * 
	 * @return
	 */
	public static Autopen getInstance(){
		return INSTANCE;
	}
	
	public CRManager getCRManager(){
		return this.crManager;
	}
	
	private void registerEvents(Listener... listeners){
		for(Listener li : listeners){
			Bukkit.getPluginManager().registerEvents(li, this);
		}
	}
	
	/**
	 * This just catches each command and tries to execute it if possible. Don't call it.
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		try {
			this.commands.execute(cmd.getName(), args, sender, sender);
		} catch (CommandPermissionsException e) {
			sender.sendMessage(Formatter.format("Access", "Access denied.", FormatType.ERROR));
		} catch (MissingNestedCommandException e) {
			sender.sendMessage(Formatter.format("Syntax", "You're missing a nested command. Try this:", FormatType.ERROR));
			sender.sendMessage(Formatter.format("Syntax", e.getUsage(), FormatType.ERROR));
		} catch (CommandUsageException e) {
			sender.sendMessage(Formatter.format("Syntax", e.getMessage() + " Try this:", FormatType.ERROR));
			sender.sendMessage(Formatter.format("Syntax", e.getUsage(), FormatType.ERROR));
		} catch (WrappedCommandException e) {
			if (e.getCause() instanceof NumberFormatException) {
				sender.sendMessage(Formatter.format("Syntax", "We needed a number, but you gave us a string!", FormatType.ERROR));
			} else {
				sender.sendMessage(Formatter.format("OH NO", "The end is upon us! Something broke.", FormatType.ERROR));
				e.printStackTrace();
			}
		} catch (CommandException e) {
			sender.sendMessage(Formatter.format("Syntax", e.getMessage(), FormatType.ERROR));
		}
		return true;
	}

	
	/**
	 * Registers the commands used in the plugin.
	 * To add a command, call: {@link CommandsManagerRegistration#register(Class)}
	 * 
	 * @see http://bukkit.org/threads/tut-using-sk89qs-command-framework.185423/
	 */
	private void setupCommands() {
		// this.cmdReg.register(SomeCommand.class)
		this.cmdReg.register(CmdAutopenParent.class);
	}
}
