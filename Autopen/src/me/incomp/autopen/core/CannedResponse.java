package me.incomp.autopen.core;

import java.util.List;
import java.util.Random;

import org.bukkit.entity.Player;

/**
 * Represents a canned response, which is defined in configuration.
 *
 * @author Incomp
 * @since Jan 4, 2017
 */
public class CannedResponse {
	private final String label;
	private final List<String> contents;
	
	public CannedResponse(String label, List<String> contents){
		this.label = label;
		this.contents = contents;
	}
	
	public String getLabel(){
		return this.label;
	}
	
	public List<String> getContents(){
		return this.contents;
	}
	
	public boolean requiresPlayer(){
		for(String msg : this.contents){
			if(msg.contains("%p")){
				return true;
			}else continue;
		}
		return false;
	}
	
	public String getMessage(Player target){
		final Random r = new Random();
		final int index = r.nextInt(this.contents.size());
		final String message = this.contents.get(index);
		if(target == null || !message.contains("%p")){
			return message;
		}else return message.replaceAll("%p", target.getName());
	}
}
