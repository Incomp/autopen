package me.incomp.autopen.core;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import me.incomp.autopen.Autopen;
import me.incomp.pith.format.Formatter;

/**
 * Describe the file here
 *
 * @author Incomp
 * @since Jan 4, 2017
 */
public class CRManager {
	private final Autopen main;
	private final Set<CannedResponse> cache = new HashSet<>();
	
	public CRManager(Autopen main){
		this.main = main;
		this.load();
	}
	
	private void load(){
		this.cache.clear();
		
		final FileConfiguration config = this.main.getConfig();
		final ConfigurationSection section = config.getConfigurationSection("responses");
		
		for(String s : section.getKeys(false)){
			// TODO: Test for validity (make sure the config is correct)
			if(section.getStringList(s + ".messages") == null){
				Formatter.log("The canned response named \"" + s + "\" is not defined properly. Skipping it.", Level.SEVERE);
				continue;
			}else{
				CannedResponse cr = new CannedResponse(s, section.getStringList(s + ".messages"));
				this.cache.add(cr);
			}
		}
	}
	
	public CannedResponse getCannedResponse(String label){
		for(CannedResponse cr : this.cache){
			if(cr.getLabel().equalsIgnoreCase(label)) return cr;
		}
		Formatter.log("Autopen couldn't find a canned response by the name of \"" + label + "\", returning null.", Level.WARNING);
		return null;
	}
	
	public boolean isCannedResponse(String label){
		return this.getCannedResponse(label) != null;
	}
	
	public Set<CannedResponse> getCannedResponses(){
		return Collections.unmodifiableSet(this.cache);
	}
}
