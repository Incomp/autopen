package me.incomp.autopen.core;

import org.bukkit.ChatColor;
import org.bukkit.permissions.Permissible;

/**
 * Describe the file here
 *
 * @author Incomp
 * @since Jan 12, 2017
 */
public enum Perm {
	// PERM("", ""),
	USE("use", "Lets players use Canned Responses."),
	LIST("list", "Lets players list all active Canned Reponses."),
	HELP("help", "Lets players use the help command from Autopen."),
	RELOAD("reload", "Lets players reload the Canned Responses configuration file.");
	
	private final String key;
	private final String desc;
	private final String node;
	
	private Perm(String key, String desc){
		this.key = key;
		this.desc = desc;
		this.node = "autopen." + key;
	}
	
	public String getKey(){
		return this.key;
	}
	
	public String getDescription(){
		return this.desc;
	}
	
	public String getNode(){
		return this.node;
	}
	
	public String getAccessMessage(Permissible permissible){
		final StringBuilder builder = new StringBuilder();
		if(permissible.hasPermission(this.getNode())){
			builder.append(ChatColor.GREEN + "✔ " + this.getNode());
		}else{
			builder.append(ChatColor.RED + "✘ " + this.getNode());
		}
		
		builder.append(ChatColor.GRAY + " (" + this.getDescription() + ")" + ChatColor.RESET);
		return builder.toString();
	}
	
	public boolean has(Permissible permissible){
		return permissible.hasPermission(this.getNode());
	}
}